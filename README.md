# Robot simulator #
A robot is placed on a board and moved forward one step each time. It can face 4 directions NORTH, WEST, SOUTH, EAST. Once it is properly placed on the board, it must not fall off the board by moving beyond the edges.
This code exercise was implemented using NodeJS with Mocha & Chai.

## Environments ##
### Development & Test ###
* **OS:** Windows 10 was used but any OS should be fine since NodeJS is supported on different platforms.
* **NodeJS** v0.12.5
* **Mocha** v2.3.4
* **Chai** v3.4.1

## System Dependencies & Configuration ##
The application just needs NodeJS to run. Please visit [nodejs.org](https://nodejs.org/) and install NodeJS for your platform.

## Application Installation Instructions ##
Just put the files in one directory. Make sure that you can execute node in that directory.

## Operating Instructions ##
The application can be run right away with existing data in data folder. It accepts a command line argument as path to a data file.

* Start application with existing data:

```
#!bash

node app.js data/input1.txt
```
* Start application with new data:
* * Create new text file containing commands "PLACE X,Y,F", "MOVE", "LEFT", "RIGHT", "REPORT". Each command is on one (1) line.
* * Pass it to the upper command line.

## Testing Instructions ##
For testing the app, Mocha and Chai are required. Install them using *npm*:

```
#!bash

npm install -g mocha
npm install chai
```

* Unit tests:
```
#!bash
## Test all the test cases defined in files in test folder.
mocha
## Test only main application file.
mocha test/app.js

## Note that upper mocha command is a shorthand version of
node mocha
## On Windows, mocha cannot be executed directly so please use node to execute it.
```

## Contributing ##
Hoai-Nam Tran [nam.anliaus@gmail.com](mailto://nam.anliaus@gmail.com)