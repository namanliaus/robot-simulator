module.exports = {
  // map strings to integer numbers
  "NORTH": 0,
  "EAST": 1,
  "SOUTH": 2,
  "WEST": 3,
  
  // convert numbers back to strings
  toText: function(dir) {
    switch(dir) {
      case 0: {
        return "NORTH";
        break;
      }
      case 1: {
        return "EAST";
        break;
      }
      case 2: {
        return "SOUTH";
        break;
      }
      case 3: {
        return "WEST";
        break;
      }
      default: {
        break;
      }
    }
  }
}