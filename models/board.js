// export Board object
module.exports = function() {
  // default values
  this.width = 5;
  this.height = 5;
  
  // set number of rows and columns
  this.setDimension = function(board, w, h) {
    board.w = w;
    board.h = h;
  }
  
  // get number of rows and columns
  this.getDimension = function(board) {
    if (board && board.w && board.h) {
      return {w: board.w, h: board.h};
    } else {
      return {w: this.width, h: this.height};
    }
  }
}