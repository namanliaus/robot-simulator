var directions = require("./directions");

module.exports = function() {
  this.onBoard = false;
  // default board that a robot is placed on 
  this.board = {width: 5, height: 5};
  
  /**
   * Set position and face of robot. SOUTH WEST corner is (0,0)
   * @param x: horizontal coordinate
   * @param y: vertical coordinate
   * @param f: numbers representing directions 0: NORTH, 1: EAST, 2: SOUTH, 3: WEST
   */
  this.place = function(robot, x, y, f) {
    
    // ignore the place setting if values are undefined or location is out of the board  
    if (x == undefined || x < 0 || x >= robot.board.w) return;
    if (y == undefined || y < 0 || y >= robot.board.h) return;
    if (f == undefined || f < 0 || f > directions.WEST) return;
    
    // accept the settings
    robot.x = x;
    robot.y = y;
    robot.f = f;
    // and mark that the robot is properly placed on the board
    robot.onBoard = true;
  };
  
  /**
   * Tell the robot to move one step forward on the direction that it is facing.
   * If the robot is not on a board then the command should be ignored.
   */
  this.move = function(robot) {
    // If the robot is not properly placed on board then ignore the command
    if (robot.onBoard === false) {
      return;
    }
    
    // check direction of the robot and move the robot 1 step forward in that direction 
    switch (robot.f) {
      case directions.NORTH: {
        // increase one step vertically
        this.place(robot, robot.x, robot.y + 1, robot.f);
        break;
      }
      case directions.SOUTH: {
        // decrease one step vertically
        this.place(robot, robot.x, robot.y - 1, robot.f);
        break;
      }
      case directions.WEST: {
        // increase one step horizontally
        this.place(robot, robot.x + 1, robot.y, robot.f);
        break;
      }
      case directions.EAST: {
        // decrease one step horizontally
        this.place(robot, robot.x - 1, robot.y, robot.f);
        break;
      }
      default: {
        // wrong input, ignore action
        break;
      }
    }
  };
  
  /**
   * Tell the robot to turn left (rotate 90 degree counter clockwise.)
   * If the robot is not on a board then the command should be ignored.
   */
  this.left = function(robot) {
    // If the robot is not properly placed on board then ignore the command
    if (robot.onBoard === false) {
      return;
    }
    
    // rotate the robot 90 degree counter clockwise
    robot.f -= 1;
    
    // correct the value if it is overflows
    while (robot.f < directions.NORTH) {
      robot.f = 4 + robot.f;
    }
  };
  
  /**
   * Tell the robot to turn right (rotate 90 degree clockwise.)
   * If the robot is not on a board then the command should be ignored.
   */
  this.right = function(robot) {
    // If the robot is not properly placed on board then ignore the command
    if (robot.onBoard === false) {
      return;
    }
    
    // rotate the robot 90 degree counter clockwise
    robot.f += 1;
    
    // correct the value if it is overflows
    robot.f = robot.f % 4;
  };
  
  /**
   * Report current position of the robot and its direction.
   * @return: an object representing position and direction of the robot.
   */
  this.report = function(robot) {
    return {x: robot.x, y: robot.y, f: robot.f, onBoard: robot.onBoard};
  }; 
}