var RobotApp = require("./RobotApp");

var app = new RobotApp();
// init board and robot
app.init();

// read command line arguments for data file path
var dataFile = "data/input2.txt";
if (process.argv.length > 2) {
  dataFile = process.argv[2];
}

// read data file for commands
app.readData(dataFile, function(err) {
  if (err) throw err;
  
  // execute commands
  app.execute(function(err) {
    app.robot.report(app.robotData);
  });
});
