var fs = require("fs");
var Robot = require("./models/robot");
var Board = require("./models/board");
var directions = require("./models/directions");

/**
 * Application reads data file and command a toy robot to move around a board.
 * Data file use new line as delimiter for commands.
 * Allowed commands are:
 * * PLACE X,Y,F where X and Y are numbers and F is one of values (NORTH, SOUTH, EAST, WEST)
 * * MOVE
 * * LEFT
 * * RIGHT
 * * REPORT 
 */
var RobotApp = function() {
  /**
   * Initialize application by creating robot and board objects
   */
  this.init = function() {
    // create new board with default dimension 5x5
    this.board = new Board();
    // create new unplaced robot and tie it with created board
    this.robot = new Robot();
    
    this.boardData = this.board.getDimension();
    this.robotData = {board: this.boardData, onBoard: false};
  };
  
  /**
   * Read data from a data file, split lines, and call the callback function when done.
   * @param path (string): file path to data file
   * @param cb (function): callback function with 1 argument (err) for passing error while reading data file.
   */
  this.readData = function(path, cb) {
    var _self = this;
    
    fs.readFile(path, "utf8", function(err, data) {
      // pass error to callback function if any
      if (err) {
        return cb(err);
      }
      
      // read data as a string and remove \r character
      // Windows users may create files with \r\n 
      var content = data.toString().replace(/\r/g, "");
      // use \n as commands separator
      _self.cqueue = content.split("\n");
      
      // callback
      cb();
    });
  };
  
  /**
   * Execute commands in queue synchronously
   * @param cb (function) callback function with 1 argument (err)
   */
  this.execute = function(cb) {
    if (!this.cqueue || !this.cqueue.length) {
      // no command to be executed, done
      return cb();
    }
    
    // get the first item in queue
    var command = this.cqueue.shift();

    // check if it is an accepted command
    if (command.substr(0, 5) === "PLACE") {
      // get parameters of the command in format PLACE X,Y,F
      var paramString = command.substr(6);
      var params = paramString.split(",");
      if (params.length === 3) {
        this.robot.place(this.robotData, parseInt(params[0]), parseInt(params[1]), directions[params[2]]);
      }
    } else if (command.substr(0, 4) === "MOVE") {
      this.robot.move(this.robotData);
    } else if (command.substr(0, 4) === "LEFT") {
      this.robot.left(this.robotData);
    } else if (command.substr(0, 5) === "RIGHT") {
      this.robot.right(this.robotData);
    } else if (command.substr(0, 6) === "REPORT") {
      var loc = this.robot.report(this.robotData);
      // report directly to console
      console.log(loc.x, loc.y, directions.toText(loc.f)); 
    } else {
      // just ignore invalid commands
    }
    
    // schedule to execute next command
    var _self = this;
    process.nextTick(function() {
      _self.execute(cb);
    });
  };
};

module.exports = RobotApp;