var expect = require("chai").expect;
var RobotApp = require("../RobotApp");
var directions = require("../models/directions");

var app = new RobotApp();

describe("Robot application", function() {
  
  it("initialize", function() {
    app.init();
    expect(app).to.have.a.property("robot");
    expect(app).to.have.a.property("board");
    expect(app.board).to.have.a.property("width", 5);
    expect(app.board).to.have.a.property("height", 5);
  });
  
  it("read data file #1", function(done) {
    // read data file and put commands into a queue
    app.readData('./data/input1.txt', function(err) {
      if (err) return done(err);

      expect(app).to.have.a.property("cqueue");
      expect(app.cqueue).to.have.length(20);

      done();
    });
  });

  it("run data #1", function(done) {
    // execute the queued commands
    app.execute(function(err) {
      if (err) return done(err);

      expect(app.cqueue).to.have.length(0);
      // check final position of the robot
      var loc = app.robot.report(app.robotData);
      expect(loc).to.deep.equal({
        x: 4,
        y: 3,
        f: directions.WEST,
        onBoard: true
      });
      
      done();
    });
  });
  
  it("read data file #2", function(done) {
    // read data file and put commands into a queue
    app.readData('./data/input2.txt', function(err) {
      if (err) return done(err);

      expect(app).to.have.a.property("cqueue");
      expect(app.cqueue).to.have.length(15);

      done();
    });
  });
  
  it("run data #2", function(done) {
    // execute the queued commands
    app.execute(function(err) {
      if (err) return done(err);

      expect(app.cqueue).to.have.length(0);
      // check final position of the robot
      var loc = app.robot.report(app.robotData);
      expect(loc).to.deep.equal({
        x: 2,
        y: 1,
        f: directions.EAST,
        onBoard: true
      });
      
      done();
    });
  });
});