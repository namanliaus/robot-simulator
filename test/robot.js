var expect = require("chai").expect;
var Robot = require("../models/robot");
var Board = require("../models/board");
var directions = require("../models/directions");

var board = new Board;
var robot = new Robot;

var boardData = board.getDimension();

// Test suit for Robot object 
describe("Robot", function() {
  
  // Test suit for PLACE command
  describe("PLACE command handling", function() {
    it("place robot correctly on board", function() {
      var robotData = {board: boardData};
      
      robot.place(robotData, 0, 0, directions.NORTH);
      expect(robotData.x).to.equal(0);
      expect(robotData.y).to.equal(0);
      expect(robotData.f).to.equal(directions.NORTH);
      expect(robotData.onBoard).to.equal(true);
      
      robot.place(robotData, 2, 4, directions.WEST);
      expect(robotData.x).to.equal(2);
      expect(robotData.y).to.equal(4);
      expect(robotData.f).to.equal(directions.WEST);
      expect(robotData.onBoard).to.equal(true);
    });
    
    it("place robot incorrectly (out of the board)", function() {
      var robotData = {onBoard: false, board: boardData};

      // invalid x value
      robot.place(robotData, -1, 0, directions.NORTH);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      // invalid y value
      robot.place(robotData, 0, -1, directions.NORTH);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      // invalid f value
      robot.place(robotData, 0, 0, -1);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      // invalid x value again
      robot.place(robotData, board.width + 1, 0, directions.SOUTH);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      // invalid y value again
      robot.place(robotData, 2, board.height, directions.WEST);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      // invalid f value again
      robot.place(robotData, 2, 3, 6);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      // put the robot on board and try to move it out
      robot.place(robotData, 2, 3, directions.NORTH);
      robot.place(robotData, -1, 5, 5);
      // the later command should be ignored so the robot stays
      // on the board with its previous position 
      expect(robotData.x).to.equal(2);
      expect(robotData.y).to.equal(3);
      expect(robotData.f).to.equal(directions.NORTH);
      expect(robotData.onBoard).to.equal(true);
    });
  });
  
  // Test suit for MOVE command
  describe("MOVE command handling", function() {
    it("move when robot is not on board", function() {
      var robotData = {onBoard: false, board: boardData};
      // new generated robot is not on the board
      // the move command should be ignore when the robot is not on the board
      
      robot.move(robotData);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
      
      robot.place(robotData, 2, 3, 6);
      robot.move(robotData);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
    });
    
    it("move horizontally", function() {
      var robotData = {board: boardData};
      // robot is facing EAST
      robot.place(robotData, 2, 1, directions.EAST);
      robot.move(robotData);
      expect(robotData.x).to.equal(1);
      expect(robotData.y).to.equal(1);
      expect(robotData.f).to.equal(directions.EAST);
      
      // robot is facing WEST
      robot.place(robotData, 2, 1, directions.WEST);
      robot.move(robotData);
      expect(robotData.x).to.equal(3);
      expect(robotData.y).to.equal(1);
      expect(robotData.f).to.equal(directions.WEST);
    });
    
    it("move vertically", function() {
      var robotData = {board: boardData};
      // robot is facing NORTH
      robot.place(robotData, 2, 1, directions.NORTH);
      robot.move(robotData);
      expect(robotData.x).to.equal(2);
      expect(robotData.y).to.equal(2);
      expect(robotData.f).to.equal(directions.NORTH);
      
      // robot is facing SOUTH
      robot.place(robotData, 2, 1, directions.SOUTH);
      robot.move(robotData);
      expect(robotData.x).to.equal(2);
      expect(robotData.y).to.equal(0);
      expect(robotData.f).to.equal(directions.SOUTH);
    });
    
    it("move horizontally when robot is at the left/right edges", function() {
      var robotData = {board: boardData};
      // robot is facing EAST and at the left edge
      robot.place(robotData, 0, 1, directions.EAST);
      robot.move(robotData);
      expect(robotData.x).to.equal(0);
      expect(robotData.y).to.equal(1);
      expect(robotData.f).to.equal(directions.EAST);
      
      // robot is facing WEST and at the right edge
      robot.place(robotData, board.width - 1, 1, directions.WEST);
      robot.move(robotData);
      expect(robotData.x).to.equal(board.width - 1);
      expect(robotData.y).to.equal(1);
      expect(robotData.f).to.equal(directions.WEST);
    });
    
    it("move vertically when robot is at the top/bottom edges", function() {
      var robotData = {board: boardData};
      // robot is facing NORTH and at the top edge
      robot.place(robotData, 2, board.height - 1, directions.NORTH);
      robot.move(robotData);
      expect(robotData.x).to.equal(2);
      expect(robotData.y).to.equal(board.height - 1);
      expect(robotData.f).to.equal(directions.NORTH);
      
      // robot is facing SOUTH and at the bottom edge
      robot.place(robotData, 2, 0, directions.SOUTH);
      robot.move(robotData);
      expect(robotData.x).to.equal(2);
      expect(robotData.y).to.equal(0);
      expect(robotData.f).to.equal(directions.SOUTH);
    });
  });
  
  // Test suit for LEFT command
  describe("LEFT command handling", function() {
    it("rotate when robot is not on board", function() {
      var robotData = {board: boardData, onBoard: false};
      robot.left(robotData);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
    });
    
    it("rotate 90 degree counter clockwise", function() {
      var robotData = {board: boardData, onBoard: false};
      robot.place(robotData, 0, 0, directions.NORTH)
      robot.left(robotData);
      expect(robotData.x).to.equal(0);
      expect(robotData.y).to.equal(0);
      expect(robotData.f).to.equal(directions.WEST);
      
      robot.left(robotData);
      expect(robotData.f).to.equal(directions.SOUTH);
      
      robot.left(robotData);
      expect(robotData.f).to.equal(directions.EAST);
      
      robot.left(robotData);
      expect(robotData.f).to.equal(directions.NORTH);
    });
  });
  
  // Test suit for RIGHT command
  describe("RIGHT command handling", function() {
    it("rotate when robot is not on board", function() {
      var robotData = {board: boardData, onBoard: false};
      robot.right(robotData);
      expect(robotData).to.not.have.a.property("x");
      expect(robotData).to.not.have.a.property("y");
      expect(robotData).to.not.have.a.property("f");
      expect(robotData.onBoard).to.equal(false);
    });
    
    it("rotate 90 degree clockwise", function() {
      var robotData = {board: boardData, onBoard: false};
      robot.place(robotData, 0, 0, directions.NORTH)
      robot.right(robotData);
      expect(robotData.x).to.equal(0);
      expect(robotData.y).to.equal(0);
      expect(robotData.f).to.equal(directions.EAST);
      
      robot.right(robotData);
      expect(robotData.f).to.equal(directions.SOUTH);
      
      robot.right(robotData);
      expect(robotData.f).to.equal(directions.WEST);
      
      robot.right(robotData);
      expect(robotData.f).to.equal(directions.NORTH);
    });
  });
  
  // Test suit for REPORT command
  describe("REPORT command handing", function() {
    it("report function", function() {
      var robotData = {board: boardData, onBoard: false};
      robot.place(robotData, 0, 0, directions.NORTH);
      var result = robot.report(robotData);
      expect(result).to.have.a.property("x", 0);
      expect(result).to.have.a.property("y", 0);
      expect(result).to.have.a.property("f", directions.NORTH);
      
      // WEST
      robot.left(robotData);
      // 1, 0
      robot.move(robotData);
      // 2, 0
      robot.move(robotData);
      // NORTH
      robot.right(robotData);
      // 2, 1
      robot.move(robotData);
      // EAST
      robot.right(robotData);
      result = robot.report(robotData);
      expect(result).to.have.a.property("x", 2);
      expect(result).to.have.a.property("y", 1);
      expect(result).to.have.a.property("f", directions.EAST);
    });
  });
});