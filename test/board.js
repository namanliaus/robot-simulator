var expect = require("chai").expect;
var Board = require("../models/board");
var board = new Board();

describe("Board", function() {
  var boardData = null;
  it("init board data", function() {
    boardData = board.getDimension();
    expect(boardData).to.deep.equal({w: 5, h: 5});
  });
  
  it("set dimension (15, 15) for board", function() {
    board.setDimension(boardData, 15, 15);
    expect(boardData).to.deep.equal({w: 15, h: 15});
  });
  
  it("get dimension", function() {
    var d = board.getDimension(boardData);
    expect(d).to.deep.equal({w: 15, h: 15});
  });
});