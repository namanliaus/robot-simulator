var expect = require("chai").expect;
var directions = require("../models/directions");

// Test suit for direction object 
describe("Directions", function() {
  it("toText function", function() {
    expect(directions.toText(0)).to.equal("NORTH");
    expect(directions.toText(1)).to.equal("EAST");
    expect(directions.toText(2)).to.equal("SOUTH");
    expect(directions.toText(3)).to.equal("WEST");
  });
});